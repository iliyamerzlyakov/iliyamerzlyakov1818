package Ex3;

import java.util.ArrayList;
import java.util.Arrays;

public class SortingArrayList {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList(Arrays.asList(5, 3, 6, 1, 2, 4, 8, 9, 7));
        System.out.println(numbers);
        System.out.println();
        sorting(numbers);
        System.out.println(numbers);
    }

    private static ArrayList<Integer> sorting(ArrayList<Integer> numbers) {
        for (int i = 0; i < numbers.size(); i++) {
            for (int j = 0; j < numbers.size() - 1; j++) {
                if (numbers.get(j) > numbers.get(j + 1)) {
                    int temp = numbers.get(j);
                    numbers.set(j, numbers.get(j + 1));
                    numbers.set(j + 1, temp);
                }
            }
        }
        return numbers;
    }
}

