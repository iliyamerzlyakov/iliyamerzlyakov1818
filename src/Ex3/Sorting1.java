package Ex3;

public class Sorting1 {
    public static void main(String[] args) {
        int[] array = {1, 3, 5, 7, 9, 2, 4, 6, 8};
        for (int value : array) {
            System.out.print(value + " ");
        }
        System.out.println();
        sorting(array);
        for (int value : array) {
            System.out.print(value + " ");
        }
    }

    private static void sorting(int[] array) {
        boolean sorted = true;
        while (sorted) {
            sorted = false;
            for (int i = array.length - 1; i > 0; i--) {
                int x;
                for (int j = 0; j < i; j++) {
                    if (array[j] > array[j + 1]) {
                        x = array[j];
                        array[j] = array[j + 1];
                        array[j + 1] = x;
                        sorted = true;
                    }
                }
            }
        }
    }
}
