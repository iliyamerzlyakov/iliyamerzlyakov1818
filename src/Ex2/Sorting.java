package Ex2;

public class Sorting {
    public static void main(String[] args) {
        int[] array = new int[10];
        for (int i = 0; i <10 ; i++) {
            array[i] = ((int)(Math.random() * 10) - 5);
            System.out.print(array[i]+" ");
        }
        System.out.println();
        sorting(array);
        for (int value : array) {
            System.out.print(value + " ");
        }
    }

    private static void sorting(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            for (int j = i + 1; j < 10; j++) {
                if (array[j] < min) {
                    min = array[j];
                    int minI = j;
                    if (i != minI) {
                        int x = array[i];
                        array[i] = array[minI];
                        array[minI] = x;
                    }
                }
            }
        }
    }
}
