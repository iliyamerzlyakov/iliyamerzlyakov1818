package Ex4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class Sorting2 {
    public static void main(String[] args) {
        int[] numb = {6, 3, 2, 1, 5, 4, 7, 9, 8};
        System.out.println(Arrays.toString(numb));
        insertionSort(numb);
        System.out.println(Arrays.toString(numb));
        List<Integer> numbers = new ArrayList<>();
        System.out.println();
        numbers = Arrays.asList(-1,-3,-5,-2,-4,0,1,3,5,2,4);
        System.out.print(numbers);
        System.out.println();
        sorting(numbers);
        System.out.print(numbers);
    }

    private static void sorting(List<Integer> numbers) {
        for (int out = 1; out < numbers.size(); out++) {
            int temp = numbers.get(out);
            int in = out;
            while (in > 0 && numbers.get(in - 1) >= temp) {
                numbers.set(in, numbers.get(in - 1));
                in--;
            }
            numbers.set(in, temp);
        }
    }
        private static void insertionSort(int[] numb) {
            for (int out = 1; out < numb.length; out++) {
                int temp = numb[out];
                int in = out;
                while (in > 0 && numb[in - 1] >= temp) {
                    numb[in] = numb[in - 1];
                    in--;
                }
                numb[in] = temp;
            }
        }
    }


