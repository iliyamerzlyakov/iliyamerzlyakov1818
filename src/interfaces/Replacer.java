package interfaces;

import interfaces.printers.IPrinter;
import interfaces.readers.IReader;

public class Replacer {
    private IReader reader;
    private IPrinter printer;

    public Replacer(IReader reader, IPrinter printer) {
        this.printer = printer;
        this.reader = reader;
    }
    void replace(){
        final String text = reader.read();
        final String replacedText = text.replaceAll(":\\)",":-(");
        printer.print(replacedText);
    }
}
