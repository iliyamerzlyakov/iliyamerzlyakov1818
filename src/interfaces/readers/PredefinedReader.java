package interfaces.readers;

import interfaces.readers.IReader;

public class PredefinedReader implements IReader {
    private String text;

    public PredefinedReader(String text) {
        this.text = text;
    }

    public String read() {
        return text;
    }
}
