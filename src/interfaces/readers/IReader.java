package interfaces.readers;

public interface IReader {
    String read();
}
