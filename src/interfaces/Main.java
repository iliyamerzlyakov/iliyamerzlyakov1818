package interfaces;

import interfaces.printers.ConsolePrinter;
import interfaces.printers.DecorativePrinter;
import interfaces.printers.IPrinter;
import interfaces.printers.advConsolePrinter;
import interfaces.readers.IReader;
import interfaces.readers.PredefinedReader;

public class Main {
    public static void main(String[] args) {
        IReader reader = new PredefinedReader("Привет:-) Пока:)");
        IPrinter printer = new ConsolePrinter();
        IPrinter advPrinter = new advConsolePrinter();
        IPrinter decoPrinter = new DecorativePrinter();
        Replacer replacer = new Replacer(reader, printer);
        Replacer advReplacer = new Replacer(reader,advPrinter);
        Replacer decoReplacer = new Replacer(reader, decoPrinter);
        replacer.replace();
        advReplacer.replace();
    }
}
