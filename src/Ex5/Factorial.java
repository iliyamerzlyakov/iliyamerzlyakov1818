package Ex5;
import java.math.BigInteger;
import java.util.Scanner;
import static java.math.BigInteger.ONE;

/**
 * Класс, для вычисления факториала с помощью метода
 * @author Merzlyakov I.
 */
public class Factorial {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите значение факториала - ");
        int f = scanner.nextInt();

        if (f<0){
            System.out.println("Введите положительное значение!");
            return;
        }
        System.out.println(factorial(f));
    }

    /**
     * Метод для вычисления факториала числа
     */
    private static BigInteger factorial(int f) {
        if (f==0 || f==1)
            return ONE;
        BigInteger m = ONE;
        for (int i = 1; i <= f ; i++) {
            m = m.multiply(BigInteger.valueOf(i));
        }
        return m;
    }
}
