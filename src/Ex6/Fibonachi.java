package Ex6;
import java.util.Scanner;
/**
 *класс для нахождения чисел Фибоначи
 *@author Мерзляков И.М.
 */
public class Fibonachi {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите порядковый номер числа Фибоначчи - ");
        int f = scanner.nextInt();
        if (f<0){
            System.out.println("Введите положительное число!");
            return;
        }
        System.out.println(fibonachi(f));
    }
    private static int fibonachi(int n) {
        int F1 = 1;
        int F_2 = 1;
        int F_n = 0;
        for (int i = 3; i < n+1; i++) {
            F_n = F1 + F_2;
            F1 = F_2;
            F_2 = F_n;
        }
        return F_n;
    }
}

