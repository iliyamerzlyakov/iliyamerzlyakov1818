package Ex7;
import java.util.ArrayList;
import java.util.Scanner;
public class SortingObjects {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        Person person2 = new Person("Волкова", 2001);
        Person person1 = new Person("Мерзляков", 2018);
        Person person3 = new Person("Прокошенко", 2002);

        ArrayList<Person> persons = new ArrayList<>();

        persons.add(person1);
        persons.add(person2);
        persons.add(person3);
        output(persons);
        search(persons);
        output(persons);
        searchName(persons);
        output(persons);
        System.out.print("Введите фамилию для поиска - ");
        String name = scanner.nextLine();
        output(searchName(persons));

    }

    private static void output(ArrayList<Person> persons) {
        System.out.println(persons);
    }

    private static ArrayList<Person> searchName(ArrayList<Person> persons, String name) {
        ArrayList<Person> newperson = new ArrayList<>();
        for (int i = 0;i<newperson.size();i++){
            if (name.equalsIgnoreCase(persons.get(i).getName())){
                newperson.add(persons.get(i));
            }
        } return newperson;
    }

    private static ArrayList<Person> search(ArrayList<Person> persons) {
        for (int i = 0; i < persons.size(); i++) {
            for (int j = i + 1; j < persons.size(); j++) {
                if (persons.get(i).getDateofbirth() < persons.get(j).getDateofbirth()) {
                    Person x = persons.get(i);
                    persons.set(i, persons.get(j));
                    persons.set(j, x);
                    continue;
                }
            }
        }
        return persons;
    }

    private static ArrayList<Person> searchName(ArrayList<Person> persons) {
        for (int i = 0; i < persons.size() -1; i++) {
            for (int j = i + 1; j < persons.size(); j++) {
                int num = (persons.get(i).getName().compareTo(persons.get(j).getName()));
                if (num > 0) {
                    Person x = persons.get(i);
                    persons.set(i, persons.get(j));
                    persons.set(j, x);
                    continue;
                }
            }
        }
        return persons;
    }
}
