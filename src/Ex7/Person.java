package Ex7;

public class Person {
    private String name;
    private int dateofbirth;

    public Person(String name, int dateofbirth) {
        this.name = name;
        this.dateofbirth = dateofbirth;
    }

    public String getName() {
        return name;
    }

    public int getDateofbirth() {
        return dateofbirth;
    }

    @Override
    public String toString() {
        return "Person{" +
                "Фамилия - '" + name + '\'' +
                ", Дата рождения - " + dateofbirth +
                '}';
    }
}
